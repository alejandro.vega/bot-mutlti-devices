import React from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, Text } from "react-native";

const styles = (isTalkBot = false) =>
  StyleSheet.create({
    container: {
      height: "auto",
      borderRadius: 30,
      paddingVertical: 5,
      paddingHorizontal: 20,
      marginHorizontal: 20,
      ...(isTalkBot
        ? { justifyContent: "flex-start", backgroundColor: "#ff02fd" }
        : { justifyContent: "flex-end", backgroundColor: "#428AF8" })
    },
    message: { color: "#ffffff", fontSize: 15 },
    hour: { color: "#ffffff", fontSize: 10, fontWeight: "600" },
    hourContainer: {
      marginTop: 5,
      ...(isTalkBot ? { alignItems: "flex-start" } : { alignItems: "flex-end" })
    }
  });

const Message = ({ id, message, hour, isTalkBot }) => (
  <View
    key={id}
    style={styles(isTalkBot).container}
    testID="Message__container"
  >
    <Text style={styles().message}>{message}</Text>
    <View style={styles(isTalkBot).hourContainer}>
      <Text style={styles().hour}>{hour}</Text>
    </View>
  </View>
);

Message.propTypes = {
  id: PropTypes.number.isRequired,
  message: PropTypes.string.isRequired,
  hour: PropTypes.string.isRequired,
  isTalkBot: PropTypes.bool
};
Message.defaultProps = { isTalkBot: false };
Message.displayName = "Message";

export default Message;
