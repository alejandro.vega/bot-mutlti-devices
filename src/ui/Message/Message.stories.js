import React from "react";
import { storiesOf } from "@storybook/react";

import Message from "./Message";

storiesOf("Message", module)
  .add("Message Poxu", () => (
    <Message id={1} message="Hola User" hour="9:00 pm" isTalkBot />
  ))
  .add("Message User", () => (
    <Message id={2} message="Hola Poxu" hour="9:01 pm" />
  ))
  .add("Short message", () => (
    <Message id={3} message="Hi" hour="9:03 pm" isTalkBot />
  ))
  .add("Large message", () => (
    <Message
      id={4}
      message="Hola Paaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaoxuaaa asdasdasdfasd fasdfsdfasdfasdfas dfasd fasdf asdfasd hadfhdsghd agsd asg asgdasdgasdgasgd sdgasdg ag asdgag asdg asdg asdgas  gas"
      hour="9:04 pm"
    />
  ));
