import React from "react";
import { storiesOf } from "@storybook/react";
import { Text, View, StyleSheet } from "react-native";

import Header from "./Header";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  text: { color: "#ffff", fontWeight: "500", fontSize: 15 }
});

storiesOf("Header", module)
  .add("Header default", () => <Header />)
  .add("Header with Title", () => <Header title="POXU Chat" />)
  .add("Header with Elements", () => (
    <Header>
      <View style={styles.container}>
        <Text style={styles.text}>POXU</Text>
      </View>
    </Header>
  ));
