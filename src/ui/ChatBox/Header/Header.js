import React from "react";
import PropTypes from "prop-types";
import { View, StyleSheet, Text } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",

    height: 25,
    backgroundColor: "#3e1051"
  },
  text: { color: "#ffff", fontWeight: "500", fontSize: 15 }
});

const Header = ({ children, style, title }) => (
  <View style={[styles.container, style]}>
    {title && <Text style={styles.text}>{title}</Text>}
    {children}
  </View>
);

Header.propTypes = {
  children: PropTypes.node,
  style: PropTypes.shape(Object),
  title: PropTypes.string
};
Header.defaultProps = {
  children: undefined,
  style: undefined,
  title: undefined
};
Header.displayName = "ChatBoxHeader";

export default Header;
