import React from "react";
import PropTypes from "prop-types";
import { ImageBackground, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%"
  }
});

const Body = ({ children, ...props }) => (
  <ImageBackground style={styles.container} {...props}>
    {children}
  </ImageBackground>
);

Body.propTypes = { children: PropTypes.node };

export default Body;
