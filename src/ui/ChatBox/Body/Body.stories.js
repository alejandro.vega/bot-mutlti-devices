import React from "react";
import { storiesOf } from "@storybook/react";
import { View, StyleSheet } from "react-native";

import Card from "../../Card";
import Message from "../../Message";
import conversacion from "../../mocks/conversacion";
import Body from "./Body";
import Polux from "../../Polux";

const styles = StyleSheet.create({
  card: { flex: 1 },
  itemIn: { alignSelf: "flex-start" },
  itemOut: { alignSelf: "flex-end" },
  item: {
    marginVertical: 2,
    flex: 1,
    flexDirection: "row"
  },
  body: { flex: 1, alignItems: "center" }
});

const MockMessage = () => (
  <Card style={styles.card}>
    {conversacion.map((message, id) => {
      const itemStyle = message.isBot ? styles.itemIn : styles.itemOut;
      return (
        <View style={[styles.item, itemStyle]}>
          <Message
            id={`${message.id}+${id}`}
            message={message.message}
            isTalkBot={message.isBot}
            hour={message.createdAt}
          />
        </View>
      );
    })}
  </Card>
);

storiesOf("Body", module)
  .add("Body background color ", () => (
    <Body style={{ backgroundColor: "#00b6ff" }}>
      <Polux source={require("../../Polux/img/normal.png")} />
      <MockMessage />
    </Body>
  ))
  .add("Body galaxia gif 1 ", () => (
    <Body
      source={require("../../../../assets/background/tec.gif")}
      style={styles.body}
    >
      <Polux source={require("../../Polux/img/alegre.png")} />
      <MockMessage />
    </Body>
  ))
  .add("Body galaxia gif 2", () => (
    <Body
      source={require("../../../../assets/background/b.gif")}
      style={styles.body}
    >
      <Polux source={require("../../Polux/img/saludo.png")} />
      <MockMessage />
    </Body>
  ))
  .add("Body galaxia gif 3", () => (
    <Body
      source={require("../../../../assets/background/caida.gif")}
      style={styles.body}
    >
      <Polux source={require("../../Polux/img/mover.png")} />
      <MockMessage />
    </Body>
  ))
  .add("Axity imagen 1", () => (
    <Body source={require("../../../brand/img/axity.jpg")} style={styles.body}>
      <Polux source={require("../../Polux/img/normal.png")} />
      <MockMessage />
    </Body>
  ))
  .add("Galaxia imagen 2", () => (
    <Body source={require("../../../brand/img/g2.png")}>
      <MockMessage />
    </Body>
  ))
  .add("Galaxia imagen 3", () => (
    <Body source={require("../../../brand/img/g3.png")}>
      <MockMessage />
    </Body>
  ))
  .add("Galaxia imagen 4", () => (
    <Body source={require("../../../brand/img/g5.png")}>
      <MockMessage />
    </Body>
  ));
