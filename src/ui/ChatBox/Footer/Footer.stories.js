import React from "react";
import { storiesOf } from "@storybook/react";

import Input from "../../Input";
import Footer from "./Footer";

storiesOf("Footer", module)
  .add("Footer default", () => <Footer />)
  .add("Footer with Elements", () => (
    <Footer>
      <Input value="hols" />
    </Footer>
  ));
