import React from "react";
import PropTypes from "prop-types";
import { View, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginVertical: 20,
    width: "80%",
    height: 60,
    flex: 1
  }
});

const Footer = ({ children, style }) => (
  <View style={[styles.container, style]}>{children}</View>
);

Footer.propTypes = { children: PropTypes.node };
Footer.defaultProps = { children: undefined };
Footer.displayName = "ChatBoxFooter";

export default Footer;
