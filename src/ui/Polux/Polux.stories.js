import React from "react";
import { storiesOf } from "@storybook/react";
import { View, StyleSheet } from "react-native";
import Polux from "./Polux";

const styles = StyleSheet.create({
  container: { flex: 1, alignItems: "center", marginVertical: 20 }
});

const Container = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

storiesOf("Polux", module)
  .add("Polux normal", () => (
    <Container>
      <Polux source={require("./img/normal.png")} />
    </Container>
  ))
  .add("Polux Alegre", () => (
    <Container>
      <Polux source={require("./img/alegre.png")} />
    </Container>
  ))
  .add("Polux Movimiento", () => (
    <Container>
      <Polux source={require("./img/mover.png")} />
    </Container>
  ))
  .add("Polux Saludo", () => (
    <Container>
      <Polux source={require("./img/saludo.png")} />
    </Container>
  ))
  .add("Polux default", () => (
    <Container>
      <Polux />
    </Container>
  ));
