import React from "react";
import PropTypes from "prop-types";
import { Image, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    height: 210,
    width: 130,
    zIndex: 2
  }
});

const Polux = ({ style, ...props }) => (
  <Image
    style={[styles.container, style]}
    resize="scale"
    resizeMode="contain"
    {...props}
    defaultSource={require("./img/normal.png")}
  />
);

Polux.propTypes = { style: PropTypes.shape(Object) };
Polux.defaultProps = { style: undefined };
Polux.displayName = "Polux";

export default Polux;
