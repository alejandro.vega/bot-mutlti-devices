import React from "react";
import { storiesOf } from "@storybook/react";
import { Text, View, StyleSheet } from "react-native";

import Card from "./Card";
import Message from "../Message";
import conversacion from "../mocks/conversacion";

const styles = StyleSheet.create({
  container: { flex: 1 },
  itemIn: { alignSelf: "flex-start" },
  itemOut: { alignSelf: "flex-end" },
  item: {
    marginVertical: 2,
    flex: 1,
    flexDirection: "row"
  }
});

storiesOf("Card", module)
  .add("Card with Message ", () => (
    <Card style={styles.container}>
      {conversacion.map((message, id) => {
        const itemStyle = message.isBot ? styles.itemIn : styles.itemOut;
        return (
          <View style={[styles.item, itemStyle]}>
            <Message
              id={`${message.id}+${id}`}
              message={message.message}
              isTalkBot={message.isBot}
              hour={message.createdAt}
            />
          </View>
        );
      })}
    </Card>
  ))
  .add("Card with Text", () => (
    <Card style={styles.container}>
      <Text>Esto es una CARD</Text>
    </Card>
  ))
  .add("Card dentro de otra card", () => (
    <Card style={styles.container}>
      <Card style={styles.container}>
        <Card value="hols" />
      </Card>
    </Card>
  ));
