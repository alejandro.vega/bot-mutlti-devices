import React from "react";
import PropTypes from "prop-types";
import { View, StyleSheet } from "react-native";

const styles = (height, width) =>
  StyleSheet.create({
    container: {
      height: height || "90%",
      width: width || "80%",
      backgroundColor: "white",
      borderWidth: 1,
      shadowColor: "#000000",
      shadowOpacity: 0.85,
      shadowRadius: 5,
      shadowOffset: {
        height: 0,
        width: 0
      },
      paddingHorizontal: 10,
      paddingVertical: 20,

      borderTopLeftRadius: 50,
      borderTopRightRadius: 50,
      borderBottomLeftRadius: 50,
      borderBottomRightRadius: 50,
      minHeight: 330,
      maxHeight: 400
    }
  });

const Card = ({ children, height, width, style, ...props }) => (
  <View style={[styles(height, width).container, style]} {...props}>
    {children}
  </View>
);

Card.propTypes = {
  children: PropTypes.node.isRequired,
  style: PropTypes.shape(Object),
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};
Card.defaultProps = {
  style: undefined,
  height: "90%",
  width: "80%"
};
Card.displayName = "Card";

export default Card;
