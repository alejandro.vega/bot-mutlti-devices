import React from "react";
import { StyleSheet, TouchableOpacity, Image } from "react-native";

const styles = StyleSheet.create({
  btnSend: {
    width: 40,
    height: 40,
    marginLeft: 15,
    alignItems: "center",
    justifyContent: "center"
  },
  iconSend: {
    width: 50,
    height: 50,
    alignSelf: "center"
  }
});

const uri =
  "https://cdn.pixabay.com/photo/2018/02/04/01/54/paper-planes-3128885_960_720.png";

const Send = ({ onPress, style, containerStyle, ...props }) => (
  <TouchableOpacity
    style={[styles.btnSend, containerStyle]}
    onPress={onPress}
    {...props}
  >
    <Image
      source={{ uri }}
      style={[styles.iconSend, style]}
      resize="scale"
      resizeMode="contain"
    />
  </TouchableOpacity>
);

export default Send;
