import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import Chat from "./Chat";
import conversacion from "../../ui/mocks/conversacion";

const actions = {
  onChangeMessage: action("onChangeMessage"),
  onAddMessage: action("onChangeMessage"),
  onDeleteAllMessage: action("onDeleteAllMessage")
};

storiesOf("Chat", module).add("Chat view", () => (
  <Chat messages={conversacion} {...actions} />
));
