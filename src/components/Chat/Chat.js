import React from "react";
import PropTypes from "prop-types";
import { View, FlatList, Dimensions, ImageBackground } from "react-native";

import { Footer } from "../../ui/ChatBox";
import Card from "../../ui/Card";
import Message from "../../ui/Message";
import Input from "../../ui/Input";
import Polux from "../../ui/Polux";
import Send from "../../ui/Icon/Send";
import getStyle from "./Chat.styles";

const styles = getStyle();

const Chat = ({ newMessage, messages, onChangeMessage, onAddMessage }) => (
  <ImageBackground
    style={styles.backgroundAxity}
    height={Dimensions.get("screen").height}
    defaultSource={require("../../brand/img/axity.jpg")}
  >
    <View style={styles.container}>
      <Polux />
      <Card style={styles.card}>
        <FlatList
          inverted
          data={messages}
          keyExtractor={message => message.ID}
          renderItem={({ item, id }) => {
            const itemStyle = item.isBot ? styles.itemIn : styles.itemOut;
            return (
              <View style={[styles.item, itemStyle]}>
                <Message
                  id={`${item.id}+${id}`}
                  message={item.message}
                  isTalkBot={item.isBot}
                  hour={item.createdAt}
                />
              </View>
            );
          }}
        />
      </Card>
      <Footer>
        <View style={styles.inputContainer}>
          <Input
            style={styles.inputs}
            placeholder="Escribe tu mensaje..."
            onChangeText={message => onChangeMessage(message)}
            value={newMessage}
            returnKeyType="send"
          />
        </View>
        <Send onPress={onAddMessage} />
      </Footer>
    </View>
  </ImageBackground>
);

Chat.propTypes = {
  newMessage: PropTypes.string,
  messages: PropTypes.shape(Object),
  onChangeMessage: PropTypes.func
};
Chat.defaultProps = {
  newMessage: "",
  onChangeMessage: undefined,
  messages: undefined
};

export default Chat;
