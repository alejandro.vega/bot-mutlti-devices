import { StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");

export default () =>
  StyleSheet.create({
    card: { flex: 1 },
    container: { flex: 1, alignItems: "center" },
    footer: {
      flexDirection: "row",
      justifyContent: "flex-start",
      alignItems: "center",
      marginVertical: 20,
      width: "80%",
      height: 60,
      flex: 1
    },
    inputContainer: {
      borderRadius: 50,
      paddingHorizontal: 15,
      borderBottomWidth: 2,
      height: 40,
      flexDirection: "row",
      flex: 1,
      backgroundColor: "white"
    },
    inputs: { flex: 1 },
    itemIn: { alignSelf: "flex-start" },
    itemOut: { alignSelf: "flex-end" },
    item: { marginVertical: 3, maxWidth: "80%", height: "100%" },
    backgroundAxity: { height, width, maxWidth: "100%", maxHeight: "100%" }
  });
