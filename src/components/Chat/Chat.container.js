import React, { Component } from "react";
import { AsyncStorage } from "react-native";

import getApiLuis from "../../api/getApiChatbot";
import createMessage from "../../core/createMessage";
import initialMessage from "../../core/initialMessage";
import withoutResults from "../../core/withoutResults";

import Chat from "./Chat";

class ChatContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newMessage: "",
      isBot: true,
      data: initialMessage
    };
  }

  componentDidMount() {
    this._retrieveData();
  }

  _storeData = async data => {
    try {
      await AsyncStorage.setItem("@Chat-bot:poxu", btoa(JSON.stringify(data)));
    } catch (error) {
      throw new Error("Unsecured message in storage", { error });
    }
  };

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem("@Chat-bot:poxu");
      if (value !== null) {
        const parsedData = JSON.parse(atob(value));
        this.setState({ data: parsedData || [] });
      }
    } catch (error) {
      return error;
    }
  };

  _deleteAllData = async () => {
    try {
      await AsyncStorage.clear();
    } catch (error) {
      throw new Error("Messages were not deleted in storage");
    }
  };

  _addMessage = () => {
    const { newMessage, data } = this.state;
    if (newMessage !== "") {
      this.setState(
        prevState => ({
          data: Object.values({
            ...prevState.data,
            ...createMessage(false, newMessage)
          }),
          isBot: false,
          newMessage: ""
        }),
        () =>
          this._storeData(data).finally(() =>
            getApiLuis(newMessage).then(({ data }) => {
              console.log({ data });
              withoutResults(data.topScoringIntent.intent, data.entities)
                ? this.onTalkBot("Sorry, solamente tengo documentos")
                : this.onTalkBot("Tengo resultados");
            })
          )
      );
    }
  };

  _deleteAllMessages = () => {
    this.setState({ data: undefined }, this._deleteAllData);
  };

  onChangeMessage = newMessage => this.setState({ newMessage });

  onTalkBot = message =>
    this.setState(prevState => ({
      data: Object.values({
        ...prevState.data,
        ...createMessage(true, message)
      }).reverse(),
      isBot: true
    }));

  render() {
    const { data, newMessage } = this.state;
    console.log({ data });
    return (
      <Chat
        messages={data}
        newMessage={newMessage}
        onChangeMessage={this.onChangeMessage}
        onAddMessage={this._addMessage}
        onDeleteAllMessage={this._deleteAllMessages}
      />
    );
  }
}

export default ChatContainer;
