import uuidv1 from "uuid/v1";

export default (isBot = false, newMessage = "") => ({
  [uuidv1()]: {
    id: uuidv1(),
    isBot,
    message: newMessage,
    createdAt: new Date().toLocaleString("es-CL", {
      hour: "numeric",
      minute: "numeric",
      hour12: true
    }),
    date: Date.now()
  }
});
