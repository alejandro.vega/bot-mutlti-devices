import createMessage from "./createMessage";

const messageBot = "Hola, ¿Que documento estás buscando?";

export default Object.values(createMessage(true, messageBot));
