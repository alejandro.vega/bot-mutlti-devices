export const globalText = (type = "documento", document = "", link = "") => ({
  download: `El ${type} de ${document} lo puedes descargar aqui ${link}`,
  search: `Encontre tú ${type} de ${document} lo puedes ver en el siguiente enlace ${link}`,
  withoutResult: `No encontre ese ${type} de ${document}, te puedo ayudar en otra cosa?`,
  saludo: "Hola, que documento estas buscando?"
});
