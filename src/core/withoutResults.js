import isEmpty from "lodash/isEmpty";

export default (topScoringIntentName = "", entities = []) =>
  topScoringIntentName === "None" || isEmpty(entities);
