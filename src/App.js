import React from "react";
import { Platform } from "react-native";

import Chat from "./components/Chat";

const App = () => <Chat />;

let hotWrapper = () => () => App;
if (Platform.OS === "web") {
  const { hot } = require("react-hot-loader");
  hotWrapper = hot;
}
export default hotWrapper(module)(App);
