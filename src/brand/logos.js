export default () => ({
  small: "https://www.axity.com/wp-content/uploads/2018/08/axity_logo.png",
  medium: "https://www.axity.com/wp-content/uploads/2018/08/axity_logo-1.png",
  all: "https://www.axity.com/wp-content/uploads/2018/08/axity_w.png",

  blanck:
    "https://www.axity.com/wp-content/uploads/2018/11/axity_logo_blanco.png"
});
