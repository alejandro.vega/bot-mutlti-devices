export default () => ({
  primary: "#ff02fd",
  secundary: "#078dfc",

  danger: "#BC0C06",
  warning: "#ff832a",
  success: "#d3cf00",
  default: "#3e1051",

  hover: "#00b6ff",
  background: "rgba(119,59,190,1)",
  font: "#ffffff"
});
