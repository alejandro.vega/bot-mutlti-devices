export default () => ({
  instagram: "https://www.instagram.com/Axity_Social/",
  facebook: "https://www.facebook.com/axity.social/",
  linkedin: "https://www.linkedin.com/company/axity/",
  youtube: "https://www.youtube.com/channel/UCGmW08WY877kr-yE3UWHT9Q",
  twitter: "https://twitter.com/axity_social?lang=es"
});
