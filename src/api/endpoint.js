import config from "./config/config";

const {
  bot: { key, url, configApi }
} = config;

export default q =>
  `${url +
    key.luis +
    "?" +
    configApi +
    "&" +
    "subscription-key=" +
    key.subscription +
    "&" +
    "q=" +
    q}`;
