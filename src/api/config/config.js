// configuracion api luis
export default {
  bot: {
    url: "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/",
    key: {
      subscription: "f65d91aea65d4c63b784fa5e920082d0",
      luis: "b8452f76-5d80-4406-a327-57e8a10ecd70"
    },
    configApi: "staging=true&verbose=true&timezoneOffset=-360"
  }
};

// https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/b8452f76-5d80-4406-a327-57e8a10ecd70?staging=true&verbose=true&timezoneOffset=-360&subscription-key=f65d91aea65d4c63b784fa5e920082d0&q=Buscar%20plantilla%20excel
