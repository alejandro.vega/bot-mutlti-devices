import axios from "axios";
import handleGetLuis from "./endpoint";

export default query => axios.get(handleGetLuis(query));
