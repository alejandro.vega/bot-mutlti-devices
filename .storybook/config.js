import { configure } from "@storybook/react";

import injectHtmlFonts from "./injectHtmlFonts";

// automatically import all files ending in *.stories.js
const req = require.context("../src", true, /.stories.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}
// Option defaults.

// Debe ser ejecutado después de establecidos todos los decoradores
configure(loadStories, module);
injectHtmlFonts();
