const getFontFaceDefinition = (font, fontFamily) =>
  `@font-face {
    src: url(${font});
    font-family: '${fontFamily}';
  }`;

const createFont = (src, fontFamily) => ({
  src,
  fontFamily
});

export default () => {
  const requireVectorIconsFont = fileName =>
    require(`react-native-vector-icons/Fonts/${fileName}`);
  const fonts = [
    createFont(
      requireVectorIconsFont("MaterialCommunityIcons.ttf"),
      "Material Design Icons"
    ),
    createFont(requireVectorIconsFont("MaterialIcons.ttf", "Material Icons")),
    createFont(requireVectorIconsFont("Octicons.ttf", "Octions"))
  ];

  const fontFaces = fonts.map(({ src, fontFamily }) =>
    getFontFaceDefinition(src, fontFamily)
  );
  fontFaces.forEach(fontFace => {
    const style = document.createElement("style");
    style.type = "text/css";

    if (style.styleSheet) {
      style.styleSheet.cssText = fontFace;
    } else {
      style.appendChild(document.createTextNode(fontFace));
    }
    document.head.appendChild(style);
  });
};
